go-sftp is a small command line tool intended to help fascilitate file transfer via ssh protocol. 

## Example Usages: 

```
go-sftp -P mypassword file user@remote.address.tld:uploads/file 
go-sftp -i .ssh/id_rsa --up file 8.8.8.8 
go-sftp -i .ssh/id_rsa --down file 8.8.8.8 
go-sftp -i .ssh/id_rsa -h my.sftp.hostname -p 3333 -U sftp-user --mode up file
```

## Flags:
 
| alias | flag           | help text |
| :---: | :------------: | :-------------------------------------------------------------------------------------------------- |
| -i    | --identityFile | Relative or Absolute path to SSH Private Key File for server Authentication                         |
| -p    | --port         | SSH Server port 																					   |
| -u    | --user         | SSH Username for remote server. If not provided, go-sftp will try using the current user's username |
| -P    | --password 	 | SSH User password for remote authentication 														   |
| -h    | --host     	 | SSH Server Hostname 																				   |
| -m    | --mode     	 | Client transport mode. Can be eithe '(u)p' or '(d)own'. See also --up and --down 				   |
| -f    | --file     	 | Path to file to transport 																		   |
| -h    | --help     	 | Show this help text 																				   |
| -F    |--ftp       	 | Enable use of unencrypted FTP protocol instead of SFTP 											   |
|       | --up       	 | Set client transport to upload. Same as --mode up 												   |
|       | --down     	 | Set client transport to download. Same as --mode down 											   |
