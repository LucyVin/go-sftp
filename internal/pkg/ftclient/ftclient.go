package ftclient

import (
	"regexp"
)

const (
	remotePattern = `((?P<User>.*?)(:(?P<Password>.*))?@)?((?P<Host>(\w+\.?){2,}):)?(?P<Path>.*)`
)

// Location describes the connection details to
// a file, including hostname, path, and user
type Location struct {
	Hostname string
	User     string
	Password string
	Path     string
	Remote   bool
}

// LocationFromString returns a struct of type `location`
// based on a standard unix-style sftp path (eg user@host:/path/to/file)
func LocationFromString(path string) Location {
	r := regexp.MustCompile(remotePattern)

	pathMap := make(map[string]string)

	search := r.FindStringSubmatch(path)

	for i, key := range r.SubexpNames() {
		if key == "" {
			continue
		}

		pathMap[key] = search[i]
	}

	var locationHost string
	var isRemote bool

	if pathMap["Host"] == "" {
		locationHost = "localhost"
		isRemote = false
	} else {
		locationHost = pathMap["Host"]
		isRemote = true
	}

	loc := Location{
		locationHost,
		pathMap["User"],
		pathMap["Password"],
		pathMap["Path"],
		isRemote,
	}

	return loc
}

// TransportPlan defines the source and target Locations
type TransportPlan struct {
	Source Location
	Target Location
}
