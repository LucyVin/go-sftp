package ftclient

import (
	"github.com/jlaffaye/ftp"
)

// FTPClient wraps common functions around ftp.ServerConn
// to allow for function parity with SFTPClient
type FTPClient struct {
	Client ftp.ServerConn
}
