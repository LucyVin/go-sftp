package main

import (
	"flag"
	"fmt"
	"os"
	"os/user"

	"gitlab.com/lucyvin/go-sftp/internal/pkg/ftclient"

	"github.com/fatih/color"
)

const (
	usageHeader = "go-sftp is a small command line tool intended to help fascilitate \n" +
		"file transfer via ssh protocol. Example Usages: \n\n" +
		"\tgo-sftp -P mypassword file user@remote.address.tld:uploads/file \n" +
		"\tgo-sftp -i .ssh/id_rsa --up file 8.8.8.8 \n" +
		"\tgo-sftp -i .ssh/id_rsa --down file 8.8.8.8 \n" +
		"\tgo-sftp -i .ssh/id_rsa -h my.sftp.hostname -p 3333 -U sftp-user --mode up file\n"
	keyUsage  = "Relative or Absolute path to SSH Private Key File for server Authentication"
	passUsage = "SSH User password for remote authentication"
	userUsage = "SSH Username for remote server.\n" +
		"\t\t\tIf not provided, go-sftp will try using the current user's username"
	hostUsage = "SSH Server Hostname"
	portUsage = "SSH Server port"
	modeUsage = "Client transport mode. Can be eithe '(u)p' or '(d)own'. See also --up and --down"
	upUsage   = "Set client transport to upload. Same as --mode up"
	downUsage = "Set client transport to download. Same as --mode down"
	helpUsage = "Show this help text"
	fileUsage = "Path to file to transport"
	ftpUsage  = "Enable use of unencrypted FTP protocol instead of SFTP"
)

var (
	keyFile    string
	password   string
	sshUser    string
	host       string
	port       int
	mode       string
	tFile      string
	help       bool
	upMode     bool
	downMode   bool
	authMethod string
	useFTP     bool
)

func init() {
	hostUser, err := user.Current()

	var hostUserName string

	if err != nil {
		hostUserName = ""
	} else {
		hostUserName = string(hostUser.Username)
	}

	flag.StringVar(&keyFile, "identityFile", "", keyUsage)
	flag.StringVar(&keyFile, "i", "", keyUsage)

	flag.StringVar(&password, "password", "", passUsage)
	flag.StringVar(&password, "P", "", passUsage)

	flag.StringVar(&sshUser, "user", hostUserName, userUsage)
	flag.StringVar(&sshUser, "u", hostUserName, userUsage)

	flag.StringVar(&host, "host", "", hostUsage)
	flag.StringVar(&host, "h", "", hostUsage)

	flag.IntVar(&port, "port", 22, portUsage)
	flag.IntVar(&port, "p", 22, portUsage)

	flag.StringVar(&mode, "mode", "", modeUsage)
	flag.StringVar(&mode, "m", "", modeUsage)

	flag.StringVar(&tFile, "file", "", fileUsage)
	flag.StringVar(&tFile, "f", "", fileUsage)

	flag.BoolVar(&help, "help", false, helpUsage)

	flag.BoolVar(&upMode, "up", false, upUsage)
	flag.BoolVar(&downMode, "down", false, downUsage)

	flag.BoolVar(&useFTP, "ftp", false, ftpUsage)
	flag.BoolVar(&useFTP, "F", false, ftpUsage)

	flag.Usage = usage

	flag.Parse()

	tail := flag.Args()

	if help == true {
		flag.Usage()
		os.Exit(0)
	}

	// validate flags

	// mode settings
	if mode != "" && !inSlice(mode, []string{"up", "u", "down", "d"}) {
		usageExit(fmt.Sprintf("Invalid mode: %s", mode), nil)
	}

	if mode != "" && (upMode || downMode) || (upMode && downMode) {
		usageExit("Can not have more than one mode set", nil)
	}

	// since we have both --up and --mode up, we need to handle
	// either to our control variable, runMode
	var runMode string

	upStrings := []string{"up", "u"}
	downStrings := []string{"down", "d"}

	switch {
	case inSlice(mode, upStrings) || upMode:
		runMode = "up"

	case inSlice(mode, downStrings) || downMode:
		runMode = "down"

	default:
		runMode = ""
	}

	if len(tail) < 2 && runMode == "" {
		usageExit("Incomplete command, target and source must be provided.", nil)
	}

	// determine the SSH auth method to pass credentials to the appropriate
	// function in order to instantiate an SSH Client
	switch {
	case keyFile != "":
		authMethod = "keyFile"

	case password != "":
		authMethod = "password"

	default:
		// default will try to use ~/.ssh/id_rsa
		authMethod = "keyFile"
		keyFile = "~/.ssh/id_rsa"
	}
}

func main() {
	tail := flag.Args()

	var fileTransport ftclient.TransportPlan

	if len(tail) > 0 {
		var locations [2]ftclient.Location

		for i, loc := range tail {
			if i > 1 {
				warnMsg := fmt.Sprintf("Extra arguments %s found, ignoring...", tail[2:])

				color.Yellow(warnMsg)
				break
			}
			locations[i] = ftclient.LocationFromString(loc)
		}

		fmt.Printf("%+v\n", locations)

		if locations[0].Remote == locations[1].Remote {
			errorMsg := fmt.Sprintf("At least one location must be local and one must be remote.")
			usageExit(errorMsg, nil)
		}

		if len(locations) == 2 {
			fileTransport.Source = locations[0]
			fileTransport.Target = locations[1]
		}

	}

	switch authMethod {
	case "keyFile":

	case "password":
	}

}

func usage() {
	usg := usageHeader
	usg += fmt.Sprintf("\n -i --identityFile\t%s", keyUsage)
	usg += fmt.Sprintf("\n -p --port\t\t%s", portUsage)
	usg += fmt.Sprintf("\n -u --user\t\t%s", userUsage)
	usg += fmt.Sprintf("\n -P --password\t\t%s", passUsage)
	usg += fmt.Sprintf("\n -h --host\t\t%s", hostUsage)
	usg += fmt.Sprintf("\n -m --mode\t\t%s", modeUsage)
	usg += fmt.Sprintf("\n -f --file\t\t%s", fileUsage)
	usg += fmt.Sprintf("\n -h --help\t\t%s", helpUsage)
	usg += fmt.Sprintf("\n -F --ftp\t\t%s", ftpUsage)
	usg += fmt.Sprintf("\n --up\t\t\t%s", upUsage)
	usg += fmt.Sprintf("\n --down\t\t\t%s", downUsage)

	fmt.Println(usg)
}

func usageExit(msg string, err error) {
	flag.Usage()

	if err != nil {
		color.Red(err.Error())
	}

	e := color.RedString(msg)

	fmt.Println(e)
	os.Exit(1)
}

func inSlice(s string, list []string) bool {
	for _, b := range list {
		if b == s {
			return true
		}
	}

	return false
}
